package com.TEAF.stepDefinition;

import io.restassured.specification.RequestSpecification;

public class Req_UD {

	private RequestSpecification param;
	
	public RequestSpecification getParam() {
		if (param!=null) {
			param=null;
		}
		
		return param;
	}

	public void setParam(RequestSpecification param) {
		this.param = param;
	}

	private RequestSpecification req;

	public RequestSpecification getReq() {
		return req;
	}

	public void setReq(RequestSpecification req) {
		this.req = req;
	}
}
