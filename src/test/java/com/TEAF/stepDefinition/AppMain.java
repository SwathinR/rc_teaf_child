package com.TEAF.stepDefinition;

import org.apache.log4j.Logger;

public class AppMain {

	private static Logger logger = Logger.getLogger(AppMain.class);

	public static void main(String[] args) {
		String xlsLoc = "D:\\Child TEAF\\childteaf\\childteaf\\src\\test\\java\\com\\TEAF\\json\\", csvLoc = "D:\\Child TEAF\\childteaf\\childteaf\\src\\test\\java\\com\\TEAF\\json\\TestDataUploaded.csv", fileLoc = "";

		fileLoc = CsvToExcel.convertCsvToXls(xlsLoc, csvLoc);
		logger.info("File Location Is?= " + fileLoc);
	}
}